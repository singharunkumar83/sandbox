package com.sandbox.jenkins.jenkinsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsdemoApplication {

	public static final String comment ="hello";
	public static void main(String[] args) {
		SpringApplication.run(JenkinsdemoApplication.class, args);
	}
}
